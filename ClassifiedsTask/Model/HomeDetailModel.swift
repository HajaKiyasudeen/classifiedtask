//
//  HomeDetailModel.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation

@objc class HomeDetailModel: NSObject {
    @objc var title: String?
    @objc var price: String?
    @objc var imageURL: [String]?
    
    @objc init(name: String?,
               price: String?,
               imageURL: [String]?) {
        self.title = name
        self.price = price
        self.imageURL = imageURL
        
        super.init()
    }
}
