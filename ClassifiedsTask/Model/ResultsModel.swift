//
//  ResultsModel.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation


// MARK: - Result
struct ResultsModel: BaseModel {
    let createdAt, price, name, uid: String?
    let imageIDS: [String]?
    let imageUrls, imageUrlsThumbnails: [String]?

    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case price, name, uid
        case imageIDS = "image_ids"
        case imageUrls = "image_urls"
        case imageUrlsThumbnails = "image_urls_thumbnails"
    }
}

extension ResultsModel {
    
    func with(
        createdAt: String?? = nil,
        price: String?? = nil,
        name: String?? = nil,
        uid: String?? = nil,
        imageIDS: [String]?? = nil,
        imageUrls: [String]?? = nil,
        imageUrlsThumbnails: [String]?? = nil
    ) -> ResultsModel {
        return ResultsModel(
            createdAt: createdAt ?? self.createdAt,
            price: price ?? self.price,
            name: name ?? self.name,
            uid: uid ?? self.uid,
            imageIDS: imageIDS ?? self.imageIDS,
            imageUrls: imageUrls ?? self.imageUrls,
            imageUrlsThumbnails: imageUrlsThumbnails ?? self.imageUrlsThumbnails
        )
    }
    
    func transfromToDetailModel() -> HomeDetailModel {
        return HomeDetailModel(name: self.name,
                               price: self.price,
                               imageURL: self.imageUrls)
    }
}
