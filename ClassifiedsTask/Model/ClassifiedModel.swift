//
//  ClassifiedModel.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation
import Alamofire

// MARK: - Welcome
struct ClassifiedModel: BaseModel {
    let results: [ResultsModel]?
    let pagination: Pagination?
}

extension ClassifiedModel {
    
    func with(
        results: [ResultsModel]?? = nil,
        pagination: Pagination?? = nil
    ) -> ClassifiedModel {
        return ClassifiedModel(
            results: results ?? self.results,
            pagination: pagination ?? self.pagination
        )
    }
}
