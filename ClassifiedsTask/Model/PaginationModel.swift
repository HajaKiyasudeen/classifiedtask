//
//  PaginationModel.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation

// MARK: - Pagination
struct Pagination: BaseModel {
    let key: String?
}

extension Pagination {
    
    func with(
        key: String?? = nil
    ) -> Pagination {
        return Pagination(
            key: key ?? self.key
        )
    }
}
