//
//  APIService+Helper.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation
import Alamofire

extension APIService {
    //MARK: WeSservice parsing helper
    enum APIError: String, Error {
        case parsingFailed
        case somethingWrong
        case noNetwork
        
        func asString() -> String {
            switch self {
            case .parsingFailed, .somethingWrong:
                return StringConstant.Common.somethingWentWrong
            case .noNetwork:
                return StringConstant.Common.noNetwork
            }
        }
    }
    
    func isNetwork(failuer: @escaping (Error?) -> Void) -> Bool {
        if let haveNetwork = Network.isConnectedToInternet {
            failuer(APIError(rawValue: haveNetwork))
            return false
        }
        
        return true
    }
}

class Network {
    
    class var isConnectedToInternet: String? {
        guard NetworkReachabilityManager()?.isReachable ?? false else {
            return StringConstant.Common.noNetwork
        }
        return nil
    }
}

extension DataRequest {
    //MARK: Base DataRequest
    
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            print("URL: \(response?.url?.absoluteString ?? "-")")
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
}

extension APIService {
    //MARK: URL Generator
    
    func homeListURL() -> String {
        return "https://ey3f2y0nre.execute-api.us-east-1.amazonaws.com/default/dynamodb-writer"
    }
}
