//
//  ClassifiedViewModel.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation

class ClassifiedViewModel : NSObject {
    
    typealias kDefaultHandler = (() -> Void)
    
    fileprivate(set) var classifiedData : ClassifiedModel? {
        didSet {
            guard self.classifiedData != nil else { return }
            self.commonError = nil
            self.bindingClassifiedViewModelToController()
        }
    }
    fileprivate(set) var commonError: Error? {
        didSet {
            guard self.commonError != nil else { return }
            self.classifiedData = nil
            self.bindingClassifiedViewModelToController()
        }
    }
    
    let bindingClassifiedViewModelToController : kDefaultHandler
    
    init(handuler: @escaping kDefaultHandler) {
        bindingClassifiedViewModelToController = handuler
        
        super.init()
        
        initiateAPICall()
    }
    
    private func initiateAPICall() {
        APIService.shared.homeListAPI { [weak self] (classifiedModel: ClassifiedModel) in
            print("success")
            DispatchQueue.main.async {
                self?.classifiedData = classifiedModel
            }
        } failuer: { [weak self] (error) in
            print("failure")
            DispatchQueue.main.async {
                self?.commonError = error
            }
        }
    }
}
