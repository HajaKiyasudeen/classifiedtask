//
//  StringConstant.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import Foundation

enum StringConstant {
    
    enum Common {
        static let appName = "ClassifiedsTask"
        static let okey = "Okey"
        static let cancel = "Cancel"
        static let noNetwork = "No network available"
        static let timeout = "Request timeout! Try again."
        static let somethingWentWrong = "Something went wrong, Please try again!"
        static let imageViewADA = "image"
    }
}
