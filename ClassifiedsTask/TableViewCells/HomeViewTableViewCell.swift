//
//  HomeViewTableViewCell.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import UIKit

class HomeViewTableViewCell: UITableViewCell {
    
    var contentBackgroundView : UIView!
    var productImageView : LoadImageViewCache!
    var nameLabel : UILabel!
    var priceLabel : UILabel!
    
    var getResultsModel : ResultsModel? {
        didSet {
            defer {
                setupADA()
            }
            
            nameLabel.text = getResultsModel?.name
            priceLabel.text = getResultsModel?.price
            
            guard let imageUrls = getResultsModel?.imageUrls else { return }
            
            if let imgUrl = URL(string: imageUrls[0]) {
                productImageView.loadImageWithUrl(imgUrl)
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setUpViews()
        removeADA()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SetUpViews
    func setUpViews() {
        
        self.contentView.backgroundColor = UIColor.white
        
        //contentBackgroundView
        self.contentBackgroundView = UIView()
        self.contentBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        self.contentBackgroundView.backgroundColor = UIColor.white
        self.contentBackgroundView.layer.cornerRadius = 7
        self.contentBackgroundView.layer.masksToBounds = true
        self.contentBackgroundView.layer.borderWidth = 0.5
        self.contentBackgroundView.layer.borderColor = UIColor(red: 222/255.0, green: 222/255.0, blue: 222/255.0, alpha: 1).cgColor
        self.contentView.addSubview(self.contentBackgroundView)
        
        //productImageView
        self.productImageView = LoadImageViewCache()
        self.productImageView.translatesAutoresizingMaskIntoConstraints = false
        self.productImageView.backgroundColor = UIColor.lightGray
        self.contentBackgroundView.addSubview(self.productImageView)
        
        //nameLabel
        self.nameLabel = UILabel()
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.nameLabel.textColor = UIColor.black
        self.nameLabel.font = UIFont.systemFont(ofSize: 17)
        self.nameLabel.numberOfLines = 2
        self.contentBackgroundView.addSubview(self.nameLabel)
        
        //priceLabel
        self.priceLabel = UILabel()
        self.priceLabel.translatesAutoresizingMaskIntoConstraints = false
        self.priceLabel.textColor = UIColor.black
        self.priceLabel.font = UIFont.boldSystemFont(ofSize: 15)
        self.contentBackgroundView.addSubview(self.priceLabel)

        self.setUpConstraints()
    }
    
    //MARK:- setUpConstraints
    func setUpConstraints() {
       
        //contentBackgroundView
        self.contentBackgroundView.leadingAnchor.constraint(equalTo: self.contentBackgroundView.superview!.leadingAnchor, constant: 12).isActive = true
        self.contentBackgroundView.trailingAnchor.constraint(equalTo: self.contentBackgroundView.superview!.trailingAnchor, constant: -12).isActive = true
        self.contentBackgroundView.topAnchor.constraint(equalTo: self.contentBackgroundView.superview!.topAnchor, constant: 12).isActive = true
        self.contentBackgroundView.bottomAnchor.constraint(equalTo: self.contentBackgroundView.superview!.bottomAnchor, constant: 0).isActive = true

        //productImageView
        self.productImageView.leadingAnchor.constraint(equalTo: self.productImageView.superview!.leadingAnchor, constant: 0).isActive = true
        self.productImageView.topAnchor.constraint(equalTo: self.productImageView.superview!.topAnchor, constant: 0).isActive = true
        self.productImageView.bottomAnchor.constraint(equalTo: self.productImageView.superview!.bottomAnchor, constant: 0).isActive = true
        self.productImageView.widthAnchor.constraint(equalToConstant: 130).isActive = true
        self.productImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        //nameLabel
        self.nameLabel.leadingAnchor.constraint(equalTo: self.productImageView.trailingAnchor, constant: 10).isActive = true
        self.nameLabel.topAnchor.constraint(equalTo: self.productImageView.topAnchor, constant: 10).isActive = true
        self.nameLabel.trailingAnchor.constraint(equalTo: self.nameLabel.superview!.trailingAnchor, constant: -10).isActive = true

        //priceLabel
        self.priceLabel.leadingAnchor.constraint(equalTo: self.nameLabel.leadingAnchor, constant: 0).isActive = true
        self.priceLabel.topAnchor.constraint(equalTo: self.nameLabel.bottomAnchor, constant: 15).isActive = true

    }
    
    func setupADA() {
        self.priceLabel.addADA(self.priceLabel.text ?? "")
        self.nameLabel.addADA(self.nameLabel.text ?? "")
        self.productImageView.addADA((self.nameLabel.text ?? "") + StringConstant.Common.imageViewADA)
    }
}
