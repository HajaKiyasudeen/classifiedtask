//
//  HomeViewController.swift
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

import UIKit

class HomeViewController : UIViewController {
    
    fileprivate var tableView : UITableView!
    
    fileprivate lazy var viewModel = ClassifiedViewModel(handuler: apiHandler)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpViews()
    }
    
    //MARK:- setUpViews
    func setUpViews() {
        let _ = viewModel // Invoke viewmodel
        
        self.view.backgroundColor = UIColor.white
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationItem.title = "Home"
                
        //tableView
        self.tableView = UITableView()
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.white
        self.tableView.register(HomeViewTableViewCell.self, forCellReuseIdentifier: HomeViewTableViewCell.reuseIdentifier)
        self.view.addSubview(self.tableView)
        
        self.setUpConstraints()
    }
    
    //MARK:- setUpV
    func setUpConstraints() {
        
        //tableView
        self.tableView.leadingAnchor.constraint(equalTo: self.tableView.superview!.leadingAnchor).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.tableView.superview!.trailingAnchor).isActive = true
        self.tableView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.tableView.superview!.bottomAnchor).isActive = true
    }
    
    fileprivate func apiHandler() {
        guard !viewModel.commonError.isTimeOutError(self) else {
            showAlert(message: viewModel.commonError?.localizedDescription ?? StringConstant.Common.somethingWentWrong)
            return
        }
        
        tableView.reloadData()
        
    }
}

//MARK:- UITableView Delegate & DataSource
extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let resultcount = viewModel.classifiedData?.results else { return 0}
        return resultcount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: HomeViewTableViewCell = UITableViewCell.cellFor(identifier: HomeViewTableViewCell.reuseIdentifier, in: tableView)
        
        guard let haveValue = viewModel.classifiedData?.results?[indexPath.row] else { return  cell }
                
        cell.getResultsModel = haveValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let haveValue = viewModel.classifiedData?.results?[indexPath.row] else { return }
        let homeDetailViewController = HomeDetailViewController()
        homeDetailViewController.homeDetailModel = haveValue.transfromToDetailModel()
        self.navigationController?.pushViewController(homeDetailViewController, animated: true)
    }
}
