//
//  HomeDetailViewController.m
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

#import "HomeDetailViewController.h"

@interface HomeDetailViewController ()

@property(nonatomic, strong) LoadImageViewCache *productImageView;
//@property(nonatomic, strong) UIImageView *productImageView;

@property(nonatomic, strong) UILabel *productNameLabel;
@property(nonatomic, strong) UILabel *productPriceLabel;

@end

@implementation HomeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpViews];
    
    [self setNavigationView];
    
    [self populateFields];
}

-(void)populateFields{
        
    if (self.homeDetailModel.title) {
        self.productNameLabel.text = self.homeDetailModel.title;
        self.navigationItem.title = self.homeDetailModel.title;
    }
    else {
        self.productNameLabel.text = @"Name not available";
    }
    
    if (self.homeDetailModel.price) {
        self.productPriceLabel.text = self.homeDetailModel.price;
    } else {
        self.productPriceLabel.text = @"Price not available";
    }

    if (self.homeDetailModel.imageURL) {
        if (self.homeDetailModel.imageURL[0]) {
            NSURL *imageURL = [NSURL URLWithString:self.homeDetailModel.imageURL[0]];
            
            [self.productImageView loadImageWithUrl:imageURL];
        }
    }
}

//MARK:- setNavigationView
-(void)setNavigationView {
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:17]}];

    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"]
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self action:@selector(backAction)];
    
    [self.navigationItem setLeftBarButtonItem:leftBarButtonItem];

}

//MARK:- setUpViews
- (void)setUpViews {
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    //productImageView
    self.productImageView = [[LoadImageViewCache alloc] init]; //[UIImageView new];
    self.productImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.productImageView.backgroundColor = UIColor.blackColor;
    [self.view addSubview:self.productImageView];
        
    
    //productNameLabel
    self.productNameLabel = [UILabel new];
    self.productNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.productNameLabel.textColor = UIColor.blackColor;
    self.productNameLabel.font = [UIFont systemFontOfSize:21];
    self.productNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.productNameLabel];

    //productPriceLabel
    self.productPriceLabel = [UILabel new];
    self.productPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.productPriceLabel.textColor = UIColor.blackColor;
    self.productPriceLabel.font = [UIFont systemFontOfSize:15];
    self.productPriceLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.productPriceLabel];

    [self setUpConstraints];
}

//MARK:- Actions
- (void)backAction {
    
    [self.navigationController popViewControllerAnimated:YES];
}

//MARK:- setUpConstraints
-(void)setUpConstraints {
    
    //productImageView
    [self.productImageView.leadingAnchor constraintEqualToAnchor:self.productImageView.superview.leadingAnchor constant:0].active = YES;
    [self.productImageView.trailingAnchor constraintEqualToAnchor:self.productImageView.superview.trailingAnchor constant:0].active = YES;
    [self.productImageView.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor constant:0].active = YES;
    [self.productImageView.heightAnchor constraintEqualToConstant:280].active = YES;
    
    //productNameLabel
    [self.productNameLabel.leadingAnchor constraintEqualToAnchor:self.productNameLabel.superview.leadingAnchor constant:0].active = YES;
    [self.productNameLabel.trailingAnchor constraintEqualToAnchor:self.productNameLabel.superview.trailingAnchor constant:0].active = YES;
    [self.productNameLabel.topAnchor constraintEqualToAnchor:self.productImageView.bottomAnchor constant:0].active = YES;

    //productPriceLabel
    [self.productPriceLabel.leadingAnchor constraintEqualToAnchor:self.productPriceLabel.superview.leadingAnchor constant:0].active = YES;
    [self.productPriceLabel.trailingAnchor constraintEqualToAnchor:self.productPriceLabel.superview.trailingAnchor constant:0].active = YES;
    [self.productPriceLabel.topAnchor constraintEqualToAnchor:self.productNameLabel.bottomAnchor constant:0].active = YES;

}


@end
