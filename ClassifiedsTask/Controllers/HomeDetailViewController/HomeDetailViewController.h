//
//  HomeDetailViewController.h
//  ClassifiedsTask
//
//  Created by Apple on 21/11/2020.
//

#import <UIKit/UIKit.h>
#import "ClassifiedsTask-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeDetailViewController : UIViewController

@property(nonatomic, strong) HomeDetailModel *homeDetailModel;

@end

NS_ASSUME_NONNULL_END
